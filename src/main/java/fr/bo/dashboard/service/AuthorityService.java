/**
 *
 */
package fr.bo.dashboard.service;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.bo.dashboard.bean.AuthoritiesUser;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;

/**
 * Service d'accès aux Autorités d'un utilisateur.
 *
 * @author $Author: loic.abemonty $
 */
@Component
public class AuthorityService {
    /** The technical logger. */
    private static final Logger LOGGER = GestionnaireTrace.getLoggerTechnique();

    /** Directory authority service. */
    @Autowired
    private IAuthorityService authorityRestService;

    /**
     * Recherche les autorités d'un utilisateur et les stocke dans un objet
     * permettant de les répartir par rôle.
     *
     * @param userId
     *            l'identifiant de l'utilisateur, non null
     * @return an object without data if nothing have been found, not null
     * @throws TechnicalException
     *             if a technical error occurs
     */
    public AuthoritiesUser retrieveUserAuthorities(final String userId) throws TechnicalException {

        List<ResponseAuthorityRoleBean> authorities = null;
        final AuthoritiesUser authoritiesUser = new AuthoritiesUser();
        try {
            authorities = this.authorityRestService.findAuthoritiesByUserId(userId);
        } catch (final Exception e) {
            LOGGER.error("Unable to access the authorities : {}.", e.getMessage());
            throw new TechnicalException(e);
        }
        if (authorities != null) {
            authorities.stream().forEach(a -> a.getRoles().stream().forEach(r -> authoritiesUser.add(r, a.getPaymentPartnerId())));
        }
        return authoritiesUser;
    }

}
