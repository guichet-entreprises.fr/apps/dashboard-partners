package fr.bo.dashboard.context;

import fr.bo.dashboard.bean.DashboardUserBean;

/**
 * ThreadContext Dashboard. XXX
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class DashboardThreadContext {

    /**
     * La constante thread local.
     */
    public static final ThreadLocal<DashboardUserBean> LOCAL_USER = new ThreadLocal<>();

    /**
     * Constructeur de la classe.
     *
     */
    private DashboardThreadContext() {
        super();
    }

    /**
     * Sets. XXX
     *
     * @param userFormsBean
     *            l'utilisateur GPart
     */
    public static void setUser(final DashboardUserBean dashboardUserBean) {
        LOCAL_USER.set(dashboardUserBean);
    }

    /**
     * Unset User. XXX
     */
    public static void unsetUser() {
        LOCAL_USER.remove();
    }

    /**
     * Gets GPartUserBean. XXX
     *
     * @return l'utilisateur GPartUserBean
     */
    public static DashboardUserBean getUser() {
        return LOCAL_USER.get();
    }

}
