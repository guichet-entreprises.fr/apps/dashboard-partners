/**
 *
 */
package fr.bo.dashboard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.bo.dashboard.context.DashboardThreadContext;
import fr.bo.dashboard.datasources.DataSourcesLoader;
import fr.bo.dashboard.interceptor.HabilitationService;
import fr.bo.dashboard.model.RecordSortEnum;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.property.LinkPropertyPlaceholder;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.record.ws.v1.model.LocalizedLabelBO;
import fr.ge.record.ws.v1.model.LocalizedStringBO;
import fr.ge.record.ws.v1.model.RecordActionBO;
import fr.ge.record.ws.v1.model.RecordActionTypeBOEnum;
import fr.ge.record.ws.v1.model.RecordBODisplay;
import fr.ge.record.ws.v1.model.RecordDataSource;
import fr.ge.record.ws.v1.model.RecordResultBO;

/**
 * Dashboard main page controller.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@Controller
public class DashboardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    @Qualifier("linksPlaceholderConfigurer")
    private LinkPropertyPlaceholder linkPlaceholder;

    @Autowired
    private DataSourcesLoader dataSourcePropertyPlaceholder;

    @Autowired
    private HabilitationService habilitationService;

    /** The Constant OBJECT_MAPPER. */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Value("${max.records.per.page:12}")
    private Integer maxRecordsDisplayed;

    @Value("${http.connection.timeout:15000}")
    private Long httpConnectionTimeout;

    @Value("${http.receive.timeout:15000}")
    private Long httpReceiveTimeout;

    /**
     * Get roles from current user from Directory.
     *
     * @return user roles
     */
    public String roles() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this.habilitationService.getRoles(DashboardThreadContext.getUser().getId()));
        } catch (IOException ex) {
            LOGGER.error("Unable to get roles from current user id [" + DashboardThreadContext.getUser().getId() + "]", ex);
            return null;
        }
    }

    /**
     * Display dashboard home page.
     *
     * @return string
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome() {
        return "redirect:/search";
    }

    /**
     * Display all the user's records according to searching text.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchText
     *            The searching text
     * @return The page to display
     * @throws TechniqueException
     *             The technical exception
     * @throws FunctionalException
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchRecords(final Model model, //
            final HttpServletRequest request, //
            final Locale locale, //
            @ModelAttribute("q") final String searchText, //
            @ModelAttribute("order") final String order, //
            @ModelAttribute("beginDate") final String beginDate, //
            @ModelAttribute("endDate") final String endDate, //
            @QueryParam("page") final Long page, //
            @QueryParam("tab") final String displayTab //
    ) throws TechniqueException, FunctionalException {
        return this.searchRecordsAndSort(model, request, locale, searchText, order, beginDate, endDate, page, displayTab);
    }

    /**
     * Display all the user's records according to searching text.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchText
     *            The searching text
     * @return The page to display
     * @throws TechniqueException
     *             The technical exception
     * @throws FunctionalException
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String initRecords(final Model model, //
            final HttpServletRequest request, //
            final Locale locale, //
            @QueryParam("q") final String searchText, //
            @QueryParam("order") final String order, //
            @QueryParam("beginDate") final String beginDate, //
            @QueryParam("endDate") final String endDate, //
            @QueryParam("page") final Long page, //
            @QueryParam("tab") final String displayTab //
    ) throws TechniqueException, FunctionalException {
        return this.searchRecordsAndSort(model, request, locale, searchText, order, null, endDate, page, displayTab);
    }

    /**
     * Method to search Records and sort them.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchText
     *            The searching text
     * @param order
     *            The order criteria
     * @param beginDate
     *            The begin date criteria
     * @param endDate
     *            The end date criteria
     * @param page
     *            The start index criteria
     * @param displayTab
     *            The tab to display
     * @return
     * @throws FunctionalException
     *             The functional exception
     * @throws TechniqueException
     *             The technical exception
     */
    private String searchRecordsAndSort(final Model model, final HttpServletRequest request, final Locale locale, final String searchText, String order, final String beginDate, final String endDate,
            final Long page, final String displayTab) throws FunctionalException, TechniqueException {

        Optional.ofNullable(searchText).filter(StringUtils::isNotEmpty).map(s -> model.addAttribute("q", s));
        Optional.ofNullable(beginDate).filter(StringUtils::isNotEmpty).map(s -> model.addAttribute("beginDate", s));
        Optional.ofNullable(endDate).filter(StringUtils::isNotEmpty).map(s -> model.addAttribute("endDate", s));

        model.addAttribute("order", Optional.ofNullable(order).filter(StringUtils::isEmpty).map(s -> RecordSortEnum.CREATED_DESC.getName()).orElse(order));
        model.addAttribute("tab",
                Optional.ofNullable(displayTab).filter(StringUtils::isNotEmpty).map(s -> s).orElse(this.dataSourcePropertyPlaceholder.getConfiguration().getDatasources().iterator().next().getKey()));
        model.addAttribute("recordBO", this.searchRecordsFromModule(locale, searchText, order, beginDate, endDate, page, displayTab));
        model.addAttribute("dashboardLinks", this.linkPlaceholder.getProps());
        model.addAttribute("page", Optional.ofNullable(page).filter(p -> null != p).orElse(1L));
        return "pages/dashboard";
    }

    /**
     * Searching for records based on filters and orders criteria.
     * 
     * @param roles
     *            The current user roles
     * @param locale
     *            The browser locale
     * @param searchText
     *            The search text value
     * @param sort
     *            The order criteria
     * @param beginDate
     *            The begin date criteria
     * @param endDate
     *            The end date criteria
     * @param page
     *            The start index
     * @param displayTab
     *            The tab to display
     * @return List of records matching input criteria
     */
    public List<RecordResultBO> searchRecordsFromModule(final Locale locale, final String searchText, final String sort, final String beginDate, final String endDate, final Long page,
            final String displayTab) {
        LOGGER.info("Searching for records with criterias searchText : {}, sort : {}, page : {}, displayTab : {}", searchText, sort, page, displayTab);
        final List<RecordResultBO> result = new ArrayList<>();
        String roles = this.roles();
        if (StringUtils.isEmpty(roles)) {
            this.dataSourcePropertyPlaceholder.getConfiguration().getDatasources().forEach(d -> result.add(this.emptyResult(d)));
        } else {
            this.dataSourcePropertyPlaceholder.getConfiguration().getDatasources().forEach(d -> {
                try {
                    // 06/01/2021 correction sur le calcul du OFFSET(
                    // startIndex)
                    final Long startIndex = (d.getKey().equals(displayTab)) ? Optional.ofNullable((page - 1) * this.maxRecordsDisplayed).orElse(0L) : 0L;
                    final Response response = ClientBuilder.newClient() //
                            .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                            .target(d.getPrivateUrl() + "/record/v2/search") //
                            .queryParam("startIndex", startIndex) //
                            .queryParam("maxResults", this.maxRecordsDisplayed) //
                            .queryParam("orders", sort) //
                            .queryParam("searchText", searchText) //
                            .queryParam("beginDate", beginDate) //
                            .queryParam("endDate", endDate) //
                            .queryParam("engineUserType", d.getEngineUserType()) //
                            .property("http.connection.timeout", this.httpConnectionTimeout) //
                            .property("http.receive.timeout", this.httpReceiveTimeout).request() //
                            .accept(MediaType.APPLICATION_JSON) //
                            .header("X-Roles", roles) //
                            .get();

                    LOGGER.debug("Response status for url '{}'", response.getStatus());
                    if (Status.OK.getStatusCode() == response.getStatus()) {
                        try {
                            final RecordResultBO recordResultBO = this.translate(locale, OBJECT_MAPPER.readValue(response.readEntity(String.class), RecordResultBO.class));
                            result.add( //
                                    recordResultBO //
                                            .setDataSource(d) //
                                            .setTotalPages((int) Math.ceil((Double.valueOf(recordResultBO.getTotalResults()) / this.maxRecordsDisplayed))) //
                                            .setPage(Optional.ofNullable(page).filter(p -> null != p).map(p -> p.intValue()).orElse(0)) //
                            );
                        } catch (final IOException ex) {
                            LOGGER.error("IOException", ex);
                            throw new TechnicalException("Unable to search for records for url [" + d.getPrivateUrl() + "]");
                        }
                    } else {
                        result.add(this.emptyResult(d));
                    }
                } catch (RuntimeException ex) {
                    LOGGER.error("Unable to search for records for url [" + d.getPrivateUrl() + "]", ex);
                    result.add(this.emptyResult(d));
                }
            });
        }
        return result;
    }

    private RecordResultBO emptyResult(final RecordDataSource source) {
        LOGGER.error("Unable to search for records for url [" + source.getPrivateUrl() + "]");
        final RecordResultBO recordResult = new RecordResultBO();
        recordResult.setRecords(new ArrayList<>());
        recordResult.setDataSource(source);
        recordResult.setTotalResults(String.valueOf(0));
        recordResult.setNbResults(String.valueOf(0));
        recordResult.setStartIndex(String.valueOf(0));
        return recordResult;
    }

    /**
     * Method searching for records.
     *
     * @param userId
     *            Id user
     * @param locale
     *            Browser locale
     * @param recordResult
     *            Records result
     */
    private RecordResultBO translate(final Locale locale, final RecordResultBO recordResultBO) {
        final RecordResultBO recordResultTranslate = recordResultBO;
        LOGGER.info("Récupération de {} dossier(s) sur un total de {}", recordResultBO.getNbResults(), recordResultTranslate.getTotalResults());
        // Definition of the labels according to the Locale of the browser
        for (final RecordBODisplay record : recordResultTranslate.getRecords()) {
            // Actions
            this.translateActions(locale, record);
            // State Label
            this.translateStateLabelBO(locale, record);
        }
        return recordResultTranslate;
    }

    /**
     * translate State Label.
     *
     * @param locale
     *            local langage
     * @param record
     *            the record
     */
    private void translateStateLabelBO(final Locale locale, final RecordBODisplay record) {
        final LocalizedLabelBO summary = record.getSummary();
        if (summary != null) {
            final List<LocalizedStringBO> summaryValues = summary.getValues();
            if (CollectionUtils.isNotEmpty(summaryValues)) {
                for (final LocalizedStringBO translation : summaryValues) {
                    if (translation.getLocale().equals(locale.getLanguage())) {
                        summary.setActualLabel(translation.getValue());
                    }
                }
            }
        }
    }

    /**
     * Translate Actions.
     *
     * @param locale
     *            local langage
     * @param record
     *            the record
     */
    private void translateActions(final Locale locale, final RecordBODisplay record) {
        for (final RecordActionBO action : record.getActions()) {
            final LocalizedLabelBO label = action.getLabel();
            for (final LocalizedStringBO translation : label.getValues()) {
                if (translation.getLocale().equals(locale.getLanguage())) {
                    label.setActualLabel(translation.getValue());
                }
            }
        }
    }

    /**
     * Execute an action for a record from the dashboard home page.
     *
     * @param model
     *            The model view
     * @param code
     *            The record identifier
     * @param action
     *            The action identifier
     * @param httpServletResponse
     *            The HttpServletResponse
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/source/{source}/code/{code}/action/{action}/execute", method = RequestMethod.GET)
    public String executeAction(final Model model, //
            @PathVariable("code") final String code, //
            @PathVariable("source") final String source, //
            @PathVariable("action") final String action, //
            final HttpServletResponse httpServletResponse) throws IOException {

        final RecordDataSource dataSource = this.dataSourcePropertyPlaceholder.getConfiguration().getDatasources().stream().filter(s -> source.equals(s.getKey())).findFirst().orElse(null);
        if (null == dataSource || StringUtils.isEmpty(dataSource.getEngineUserType()) || StringUtils.isEmpty(dataSource.getPublicUrl()) || StringUtils.isEmpty(dataSource.getPrivateUrl())) {
            // -->Back to the home page
            return "redirect:/";
        }

        final String roles = this.roles();
        if (StringUtils.isEmpty(roles)) {
            // -->Back to the home page
            return "redirect:/";
        }

        final Response execResponse = ClientBuilder.newClient() //
                .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                .target(String.format("%s/v1/Record/code/%s/action/%s/execute", dataSource.getPrivateUrl(), code, action)) //
                .queryParam("engineUser", dataSource.getEngineUserType()) //
                .request() //
                .accept(MediaType.APPLICATION_JSON) //
                .header("X-Roles", roles) //
                .post(null);
        if (null != execResponse && Status.OK.getStatusCode() == execResponse.getStatus()) {
            if (RecordActionTypeBOEnum.isExecAction(action) || !RecordActionTypeBOEnum.isConfirmAction(action)) {
                final StringBuilder urlRedirection = new StringBuilder(dataSource.getPublicUrl());
                urlRedirection.append(execResponse.readEntity(String.class));
                return "redirect:" + urlRedirection.toString();
            }
        }
        // -->Redirection to the home page
        return "redirect:/";
    }

    /**
     * Display all the user's records according to searching text.
     *
     * @param model
     *            The model
     * @param request
     *            The HTTP request
     * @param locale
     *            The browser locale
     * @param searchText
     *            The searching text
     * @return The page to display
     * @throws TechniqueException
     *             The technical exception
     * @throws FunctionalException
     */
    @RequestMapping(value = "/page/{tab}", method = RequestMethod.GET)
    public String page(final Model model, //
            final HttpServletRequest request, //
            final Locale locale, //
            @PathVariable("tab") final String displayTab, //
            @QueryParam("page") final Long page, //
            @QueryParam("order") final String order, //
            @QueryParam("beginDate") final String beginDate, //
            @QueryParam("endDate") final String endDate, //
            @QueryParam("q") final String searchText //
    ) throws TechniqueException, FunctionalException {
        return this.searchRecordsAndSort(model, request, locale, searchText, order, beginDate, endDate, page, displayTab);
    }
}