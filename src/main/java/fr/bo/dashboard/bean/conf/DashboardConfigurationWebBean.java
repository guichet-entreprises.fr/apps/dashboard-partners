/**
 *
 */
package fr.bo.dashboard.bean.conf;

import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Configuration class for Dashboard. TODO agréger les conf des deux
 * applications filles
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class DashboardConfigurationWebBean implements Serializable {

    /** UID. */
    private static final long serialVersionUID = 6702179590607766389L;

    /** Static instance of the DashboardConfigurationWebBean bean. */
    private static DashboardConfigurationWebBean instance = new DashboardConfigurationWebBean();

    /** The URL for the NASH server. */
    private String urlNash;

    /** The URL for the NASH WS server. */
    private String urlNashServer;

    /** The URL that checks if the forms server is alive. */
    private String urlNashServerAlive;

    /** The number of max results for GE. */
    private String maxResult;

    /** The URL for the feedback user. */
    private String urlFeedback;

    /** The host for the feedback user. */
    private String hostFeedback;

    /** The URL that checks if directory-ws server is up. */
    private String urlDirectoryServerAlive;

    /**
     * Retrieves the unique instance of the DashboardConfigurationWebBean.
     *
     * @return the unique instance of the DashboardConfigurationWebBean
     */
    public static DashboardConfigurationWebBean getInstance() {
        return instance;
    }

    /**
     * Encode the input URL using UTF-8 encoding.
     *
     * @param url
     *            the url
     * @return The URL encoded using UTF-8 or null
     */
    private String encodeURL(final String url) {
        try {
            return URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * Accesseur sur l'attribut {@link #urlNash}.
     *
     * @return url nash
     */
    public String getUrlNash() {
        return this.urlNash;
    }

    /**
     * Mutateur sur l'attribut {@link #urlNash}.
     *
     * @param urlNash
     *            la nouvelle valeur de l'attribut urlNash
     */
    public void setUrlNash(final String urlNash) {
        this.urlNash = urlNash;
    }

    /**
     * Gets the url nash server.
     *
     * @return the url nash server
     */
    public String getUrlNashServer() {
        return this.urlNashServer;
    }

    /**
     * Sets the url nash server.
     *
     * @param urlNashServer
     *            the new url nash server
     */
    public void setUrlNashServer(final String urlNashServer) {
        this.urlNashServer = urlNashServer;
    }

    /**
     * Gets the url nash server alive.
     *
     * @return the url nash server alive
     */
    public String getUrlNashServerAlive() {
        return this.urlNashServerAlive;
    }

    /**
     * Sets the url nash server alive.
     *
     * @param urlNashServerAlive
     *            the new url nash server alive
     */
    public void setUrlNashServerAlive(final String urlNashServerAlive) {
        this.urlNashServerAlive = urlNashServerAlive;
    }

    /**
     * Accesseur sur l'attribut {@link #maxResult}.
     *
     * @return int maxResultGE
     */
    public int getMaxResultGEFormatted() {
        // TODO LAB : le getter ne doit jamais planter, jamais. Retourner une
        // valeur par défaut avec un
        // log, sinon refaire le set ...
        return Integer.parseInt(this.maxResult);
    }

    /**
     * Sets the max result.
     *
     * @param maxResult
     *            the new max result
     */
    public void setMaxResult(final String maxResult) {
        this.maxResult = maxResult;
    }

    /**
     * Accesseur sur l'attribut {@link #urlFeedack}.
     *
     * @return String urlFeedack
     */
    public String getUrlFeedback() {
        return this.urlFeedback;
    }

    /**
     * Mutateur sur l'attribut {@link #urlFeedack}.
     *
     * @param urlFeedback
     *            the new url feedback
     */
    public void setUrlFeedback(final String urlFeedback) {
        this.urlFeedback = urlFeedback;
    }

    /**
     * Accesseur sur l'attribut {@link #hostFeedback}.
     *
     * @return String hostFeedback
     */
    public String getHostFeedback() {
        return this.hostFeedback;
    }

    /**
     * Mutateur sur l'attribut {@link #hostFeedback}.
     *
     * @param hostFeedback
     *            la nouvelle valeur de l'attribut hostFeedback
     */
    public void setHostFeedback(final String hostFeedback) {
        this.hostFeedback = hostFeedback;
    }

    /**
     * Accesseur sur l'attribut {@link #urlDirectoryServerAlive}.
     *
     * @return String urlDirectoryServerAlive
     */
    public String getUrlDirectoryServerAlive() {
        return this.urlDirectoryServerAlive;
    }

    /**
     * Mutateur sur l'attribut {@link #urlDirectoryServerAlive}.
     *
     * @param urlDirectoryServerAlive
     *            la nouvelle valeur de l'attribut urlDirectoryServerAlive
     */
    public void setUrlDirectoryServerAlive(final String urlDirectoryServerAlive) {
        this.urlDirectoryServerAlive = urlDirectoryServerAlive;
    }

}
