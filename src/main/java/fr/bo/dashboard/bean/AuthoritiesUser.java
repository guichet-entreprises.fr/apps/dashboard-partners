/**
 *
 */
package fr.bo.dashboard.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * TODO javadoc
 *
 * @author $Author: loic $
 * @version $Revision: 0 $
 */
public class AuthoritiesUser {

    /**
     * List the user's authorities ids by role. Do not contains the authorities
     * without role.
     */
    private final Map<String, List<String>> authoritiesByRole = new HashMap<>();

    /** List the user's authorities without role. */
    private final List<String> authoritiesWithoutRole = new ArrayList<>();

    /**
     * Constructor.
     */
    public AuthoritiesUser() {
        // Nothing to do.
    }

    /**
     * Add an authority with a role for a user.
     *
     * @param role
     *            the title of the role, nullable
     * @param authorityId
     *            the id of the authority, discarded if null
     */
    public void add(final String role, final String authorityId) {
        if (StringUtils.isNotEmpty(role) && StringUtils.isNotEmpty(authorityId)) {
            List<String> authorities = this.authoritiesByRole.get(role);
            if (authorities == null) {
                authorities = new ArrayList<>();
                authorities.add(authorityId);
                this.authoritiesByRole.put(role, authorities);
            } else if (authorities.contains(authorityId) == false) {
                // the authority has not been added yet
                authorities.add(authorityId);
            }
        } else if (!this.authoritiesWithoutRole.contains(authorityId)) {
            // if the authority is not in the list without role yet
            this.authoritiesWithoutRole.add(authorityId);
        }
    }

    /**
     * Access to the authorities for which the user has this role. If the role
     * is null, it returns the authorities for which the user has no role.
     *
     * @param role
     *            the role, nullable
     * @return the list of the authorities ids for the role, null if not found
     */
    public List<String> getAuthorities(final String role) {
        if (StringUtils.isNotEmpty(role)) {
            List<String> authorities = this.authoritiesByRole.get(role);
            if (authorities == null) {
                authorities = new ArrayList<>();
            }
            return authorities;
        }
        return this.authoritiesWithoutRole;
    }

    /** Roles known to be used to categorize the authorities' users. */
    public interface Role {
        /** an authority referent. */
        String REFERENT_ROLE = "referent";
        /** the accountant of an authority. */
        String ACCOUNTANT_ROLE = "accountant";
        /** a simple suer. */
        String SIMPLE_USER_ROLE = "user";
    }

}
