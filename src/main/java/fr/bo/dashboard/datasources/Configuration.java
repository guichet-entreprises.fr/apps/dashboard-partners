/**
 * 
 */
package fr.bo.dashboard.datasources;

import java.util.List;

import fr.ge.record.ws.v1.model.RecordDataSource;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class Configuration {
    private List<RecordDataSource> datasources;

    @Override
    public String toString() {
        return new StringBuilder() //
                .append(String.format("Supported datasources: %s\n", datasources)) //
                .toString() //
        ;
    }

    /**
     * Accesseur sur l'attribut {@link #datasources}.
     *
     * @return List<DataSource> datasources
     */
    public List<RecordDataSource> getDatasources() {
        return datasources;
    }

    /**
     * Mutateur sur l'attribut {@link #datasources}.
     *
     * @param datasources
     *            la nouvelle valeur de l'attribut datasources
     */
    public void setDatasources(List<RecordDataSource> datasources) {
        this.datasources = datasources;
    }
}
