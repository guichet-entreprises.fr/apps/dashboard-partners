/**
 *
 */
package fr.bo.dashboard.datasources;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class DataSourcesLoader {

    private Configuration configuration;

    private String name;

    /** The Constant OBJECT_MAPPER. */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public void init() {
        try (final InputStream in = DataSourcesLoader.class.getClassLoader().getResourceAsStream(this.name)) {
            this.configuration = OBJECT_MAPPER.readValue(in, Configuration.class);
        } catch (IOException ex) {
            throw new IllegalStateException(String.format("Unable to load json configuration file '%s'", this.name), ex);
        }

    }

    /**
     * Accesseur sur l'attribut {@link #configuration}.
     *
     * @return Configuration configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Mutateur sur l'attribut {@link #configuration}.
     *
     * @param configuration
     *            la nouvelle valeur de l'attribut configuration
     */
    public void setConfiguration(final Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Mutateur sur l'attribut {@link #name}.
     *
     * @param name
     *            la nouvelle valeur de l'attribut name
     */
    public void setName(final String name) {
        this.name = name;
    }
}
