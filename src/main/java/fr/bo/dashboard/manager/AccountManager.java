/**
 *
 */
package fr.bo.dashboard.manager;

import fr.bo.dashboard.bean.DashboardUserBean;
import fr.bo.dashboard.context.DashboardThreadContext;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.manager.IAccountManager;

/**
 * AccountManager est la classe d'intégration du ct-authentification. FIXME
 * anglais
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class AccountManager implements IAccountManager<DashboardUserBean> {

    /**
     * {@inheritDoc}
     */
    @Override
    public DashboardUserBean getUserBean(final String idUser, final AccountUserBean accountUserBean) {
        final DashboardUserBean userBean = new DashboardUserBean(idUser);
        if (null != accountUserBean) {
            userBean //
                    .setNom(accountUserBean.getLastName()) //
                    .setPrenom(accountUserBean.getFirstName());
        }

        return userBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persistUserContext(final DashboardUserBean userBean) {
        // Persistance dans le thread local
        DashboardThreadContext.setUser(userBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanUserContext() {
        // on vide le context
        DashboardThreadContext.unsetUser();
    }

}
