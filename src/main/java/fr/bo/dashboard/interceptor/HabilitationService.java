package fr.bo.dashboard.interceptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;

/**
 * Get user habilitations from Directory.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class HabilitationService {

    /** Default record role. **/
    private static final String DEFAULT_RECORD_ROLE = "*";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HabilitationService.class);

    @Autowired
    @Qualifier("defaultJaxbJsonProvider")
    private JacksonJaxbJsonProvider jsonProvider;

    @Value("${ws.directory.private.url}")
    private String directoryPrivateBaseUrl;

    private static final String USER_PREFIX_ROLES = "GE/";

    /**
     * Getting access rights for an user. The access rights is the union between
     * default access rights and access rights returning by from Directory.
     *
     * @param userId
     *            the user identifier
     * @return the roles assigned to the user
     */
    public Map<String, List<String>> getRoles(final String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }

        final Map<String, List<String>> rights = new HashMap<String, List<String>>();
        try {
            final AuthorityServicesWebService service = JAXRSClientFactory.create(this.directoryPrivateBaseUrl, AuthorityServicesWebService.class, Arrays.asList(this.jsonProvider));
            rights.putAll(Optional.ofNullable(service.findUserRights(userId)) //
                    .orElse(new HashMap<String, List<String>>()));
        } catch (final Exception e) {
            // -->An error occured when calling Directory to find access rights
            LOGGER.warn("Cannot access to user rights when calling Directory. The current user can only access to its own records", e);
        }

        // -->Adding default access rights and access rights returning by
        // Directory above
        final List<String> accessRights = new ArrayList<>();
        accessRights.add(DEFAULT_RECORD_ROLE);
        if (null != rights.get(USER_PREFIX_ROLES + userId)) {
            accessRights.addAll(rights.get(USER_PREFIX_ROLES + userId));
        }
        rights.put(USER_PREFIX_ROLES + userId, accessRights);

        LOGGER.debug("Rights found for current user : {}", rights);
        return rights;
    }
}
