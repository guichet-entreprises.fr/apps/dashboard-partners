package fr.bo.dashboard.constant;

/**
 * Interface for controllers constants. TODO déplacer dans le bon package
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IControllerConstantes {

    /** The constant USER_CONNECTED. XXX explain */
    String USER_CONNECTED = "dashboard_user_connected";

    /** The name of the error page. */
    String ERROR_VIEW = "pages/error";
}
