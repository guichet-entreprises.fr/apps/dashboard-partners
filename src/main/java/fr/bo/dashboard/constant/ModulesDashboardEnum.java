/**
 *
 */
package fr.bo.dashboard.constant;

/**
 * Enumeration of all the modules that Dashboard call to get records
 * information.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum ModulesDashboardEnum {

    /** Module BACK-OFFICE. */
    BO;

}
