/**
 *
 */
package fr.bo.dashboard.model;

/**
 * Enumeration of all critera to sort GE/GQ record.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum RecordSortEnum {

    /** Sort by creation date ASC. */
    CREATED_ASC("createdAsc"),

    /** Sort by creation date Desc. */
    CREATED_DESC("createdDesc"),

    /** Sort by name. */
    TITLE("title");

    /** the name of the criteria. */
    private String name;

    /**
     * Constructor.
     *
     * @param typeActionName
     *            type action name
     */
    RecordSortEnum(final String name) {
        this.name = name;
    }

    /**
     * Accessor on {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

}
