package fr.bo.dashboard.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.bo.dashboard.bean.AuthoritiesUser;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;

public class AuthorityServiceTest {

    private static final String REFERENT_ROLE = "referent";

    private static final String USER_ROLE = "user";

    @Mock
    private IAuthorityService authorityRestService;

    @InjectMocks
    private AuthorityService authorityService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    public static ResponseAuthorityRoleBean createResponseAuthorityBean(final String funcId, final String path, final String partnerId, final String... roles) {
        final ResponseAuthorityRoleBean responseAuthority = new ResponseAuthorityRoleBean();
        responseAuthority.setEntityId(funcId);
        responseAuthority.setPath(path);
        responseAuthority.setPaymentPartnerId(partnerId);
        responseAuthority.setRoles(Stream.of(roles).collect(Collectors.toList()));
        return responseAuthority;
    }

    /**
     * No authorities found. Null version.
     */
    @Test
    public void testAuthorities2Authorities() throws Exception {
        final String userId = "2018-13-AAA-ZZ-01";

        final List<ResponseAuthorityRoleBean> responseAuthorities = new ArrayList<>();
        responseAuthorities.add(createResponseAuthorityBean("CMA75", "/GE/CMA/CMA75", "123456789123456789-01", REFERENT_ROLE));
        responseAuthorities.add(createResponseAuthorityBean("CMA76", "/GE/CMA/CMA76", "123456789123456789-02", USER_ROLE, REFERENT_ROLE, USER_ROLE));

        when(this.authorityRestService.findAuthoritiesByUserId(userId)).thenReturn(responseAuthorities);

        final AuthoritiesUser authoritiesUser = this.authorityService.retrieveUserAuthorities(userId);

        assertThat(authoritiesUser.getAuthorities(REFERENT_ROLE)).hasSize(2);
        assertThat(authoritiesUser.getAuthorities(REFERENT_ROLE)).contains("123456789123456789-01", "123456789123456789-02");
        assertThat(authoritiesUser.getAuthorities(USER_ROLE)).hasSize(1);
        assertThat(authoritiesUser.getAuthorities(USER_ROLE)).contains("123456789123456789-02");
    }

    /**
     * No authorities found. Null version.
     */
    @Test
    public void testAuthoritiesNoResult() throws Exception {
        final String userId = "2018-13-AAA-ZZ-01";
        when(this.authorityRestService.findAuthoritiesByUserId(userId)).thenReturn(null);

        final AuthoritiesUser authoritiesUser = this.authorityService.retrieveUserAuthorities(userId);

        assertThat(authoritiesUser.getAuthorities(REFERENT_ROLE)).isEmpty();
    }

    /**
     * No authorities found. Empty version.
     */
    @Test
    public void testAuthoritiesNoResultEmpty() throws Exception {
        final String userId = "2018-13-AAA-ZZ-01";
        when(this.authorityRestService.findAuthoritiesByUserId(userId)).thenReturn(new ArrayList<>());

        final AuthoritiesUser authoritiesUser = this.authorityService.retrieveUserAuthorities(userId);

        assertThat(authoritiesUser.getAuthorities(REFERENT_ROLE)).isEmpty();
    }

}
