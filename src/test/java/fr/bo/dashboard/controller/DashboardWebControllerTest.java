/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.bo.dashboard.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.bo.dashboard.bean.AuthoritiesUser;
import fr.bo.dashboard.bean.DashboardUserBean;
import fr.bo.dashboard.context.DashboardThreadContext;
import fr.bo.dashboard.interceptor.HabilitationService;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityRoleBean;
import fr.ge.directory.ws.v1.service.IAuthorityService;
import fr.ge.record.ws.v1.model.RecordBODisplay;
import fr.ge.record.ws.v1.model.RecordResultBO;
import fr.ge.record.ws.v1.rest.IRecordBOService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-bo-dashboard-webapp-config.xml", "/spring/test-client-context.xml" })
@WebAppConfiguration
public class DashboardWebControllerTest extends AbstractTest {

    private static final String RECORD_UID = "2019-03-REC-ORD-42";

    /** user id de test */
    private static final String USER_ID = "2018-03-AAA-ZZ-55";

    /** view action identifier. */
    private static final String ACTION_VIEW_ID = "view";

    /** archived action identifier. */
    private static final String ACTION_ARCHIVED_ID = "exec:archived";

    /** source identifier. */
    private static final String SOURCE_ID = "backoffice";

    /** the main object */
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private IAuthorityService authorityRestService;

    @Autowired
    private IAccountFacade accountFacade;

    @Autowired
    private IRecordBOService recordBOService;

    @Autowired
    private HabilitationService habilitationService;

    /** The endpoint. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    /** The Constant OBJECT_MAPPER. */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        reset(this.recordBOService, this.authorityRestService, this.accountFacade, this.habilitationService);

        final DashboardUserBean dashboardUserBean = new DashboardUserBean(USER_ID);
        DashboardThreadContext.setUser(dashboardUserBean);

        final AccountUserBean geUserBean = new AccountUserBean();
        geUserBean.setTrackerId(USER_ID);
        final DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
        geUserBean.setConnectionDate(df.format(new Date()));
        when(this.accountFacade.readUser(anyString())).thenReturn(geUserBean);
        final Map<String, List<String>> roles = new HashMap<>();
        roles.put(USER_ID, Arrays.asList("referent", "accountant"));
        when(this.habilitationService.getRoles(anyString())).thenReturn(roles);

        this.setUpRestServer();
    }

    @Test
    public void testHome() throws Exception {
        this.mvc.perform(get("/")) //
                .andExpect(MockMvcResultMatchers.redirectedUrl("/search"));
    }

    /**
     * Sets the up rest server.
     *
     * @throws Exception
     *             the exception
     */
    private void setUpRestServer() throws Exception {
        this.service = mock(ITestService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        if (null != this.server) {
            this.server.stop();
            this.server.destroy();
        }
    }

    private static ResponseAuthorityRoleBean createResponseAuthorityBean(final String funcId, final String path, final String partnerId, final String... roles) {
        final ResponseAuthorityRoleBean responseAuthority = new ResponseAuthorityRoleBean();
        responseAuthority.setEntityId(funcId);
        responseAuthority.setPath(path);
        responseAuthority.setPaymentPartnerId(partnerId);
        responseAuthority.setRoles(Stream.of(roles).collect(Collectors.toList()));
        return responseAuthority;
    }

    /**
     * Search BO records.
     *
     * @throws RestResponseException
     */
    @Test
    public void testSearchRecordsGet() throws Exception {

        final byte[] recordsAsJson = this.resourceAsBytes("records.json");
        final RecordResultBO records = OBJECT_MAPPER.readValue(recordsAsJson, RecordResultBO.class);
        when(this.service.searchRecordsBO(0, 100, "createdDesc", null, null, null, false, null)).thenReturn(records);

        this.mvc.perform(get("/search")) //
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testSearchRecordsPost() throws Exception {

        final RecordResultBO records = new RecordResultBO();
        records.setNbResults("1");
        final RecordBODisplay record = new RecordBODisplay();
        record.setActions(new ArrayList<>());
        records.setRecords(Arrays.asList(record));
        when(this.recordBOService.searchRecordsBO(0, 100, "createdDesc", "", "", "", false, "bo")).thenReturn(records);

        final List<ResponseAuthorityRoleBean> responseAuthorities = new ArrayList<>();
        responseAuthorities.add(createResponseAuthorityBean("CMA75", "/GE/CMA/CMA75", "123456789123456789-01", AuthoritiesUser.Role.ACCOUNTANT_ROLE));
        when(this.authorityRestService.findAuthoritiesByUserId(anyString())).thenReturn(responseAuthorities);

        this.mvc.perform(post("/search")) //
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUserError() throws Exception {
        final RecordResultBO records = new RecordResultBO();
        records.setNbResults("0");
        records.setRecords(Collections.emptyList());

        when(this.recordBOService.searchRecordsBO(0, 100, "createdDesc", null, null, null, false, "bo")).thenReturn(records);

        final List<ResponseAuthorityRoleBean> responseAuthorities = new ArrayList<>();
        responseAuthorities.add(createResponseAuthorityBean("CMA75", "/GE/CMA/CMA75", "123456789123456789-01", AuthoritiesUser.Role.ACCOUNTANT_ROLE));
        when(this.authorityRestService.findAuthoritiesByUserId(anyString())).thenReturn(responseAuthorities);

        reset(this.accountFacade);
        when(this.accountFacade.readUser(anyString())).thenThrow(RuntimeException.class);

        this.mvc.perform(get("/search")) //
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * The Interface ITestService.
     */
    public static interface ITestService {

        /**
         * Search Records For DASHBOARD BACK-OFFICE.
         * 
         * @param startIndex
         *            the start index
         * @param maxResults
         *            the number of record per page
         * @param orders
         *            the sort criteria
         * @param fullTextSearchCriteria
         *            the text to search
         * @param engineUserType
         *            the engine user type
         * @return a list of BACK-OFFICE records
         */
        @GET
        @Produces({ MediaType.APPLICATION_JSON })
        @Path("/record/v2/search")
        RecordResultBO searchRecordsBO(@QueryParam("startIndex") int startIndex, @QueryParam("maxResults") int maxResults, @QueryParam("orders") String orders,
                @QueryParam("searchText") String fullTextSearchCriteria, @QueryParam("beginDate") String beginDate, @QueryParam("endDate") String endDate, @QueryParam("archived") Boolean archived,
                @QueryParam("engineUserType") String engineUserType);

        /**
         * Execute an identified action for a record.
         *
         * @param code
         *            the record identifier
         * @param action
         *            the action identifier
         * @param engineUser
         *            the engine user
         * @return the response
         */
        @POST
        @Path("/v1/Record/code/{code}/action/{action}/execute")
        @Produces(MediaType.APPLICATION_JSON)
        Response executeAction(@PathParam("code") String code, @PathParam("action") String action, @QueryParam("engineUser") @DefaultValue("") String engineUser);
    }

    @Test
    public void testExecuteAction() throws Exception {
        when(this.service.executeAction(anyString(), anyString(), anyString())).thenReturn(Response.ok("/record/" + RECORD_UID).build());
        this.mvc.perform(MockMvcRequestBuilders.get("/source/{source}/code/{code}/action/{action}/execute", SOURCE_ID, RECORD_UID, ACTION_VIEW_ID)) //
                .andExpect(status().is3xxRedirection()) //
                .andReturn();
    }

    @Test
    public void testExecuteActionStep() throws Exception {
        when(this.service.executeAction(anyString(), anyString(), anyString())).thenReturn(Response.ok("/record/" + RECORD_UID).build());
        this.mvc.perform(MockMvcRequestBuilders.get("/source/{source}/code/{code}/action/{action}/execute", SOURCE_ID, RECORD_UID, ACTION_ARCHIVED_ID)) //
                .andExpect(status().is3xxRedirection()) //
                .andReturn();
    }
}
