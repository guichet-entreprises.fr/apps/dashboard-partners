package fr.bo.dashboard.manager;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import fr.bo.dashboard.bean.DashboardUserBean;
import fr.bo.dashboard.context.DashboardThreadContext;
import fr.bo.dashboard.manager.AccountManager;
import fr.ge.ct.authentification.bean.AccountUserBean;

public class AccountManagerTest {

    /** La constante UID_USER. */
    private static final String UID_USER = "2016-11-VHB-LKD-55";

    /** account manager. */
    @InjectMocks
    private AccountManager accountManager;

    /**
     * Injection of mocks.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Methode de test de getUserBean.
     */
    @Test
    public void getUserBean() {

        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("John");
        accountUserBean.setLastName("DOE");
        accountUserBean.setEmail("john.doe@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33612345678");
        accountUserBean.setAnonymous(false);

        final DashboardUserBean dashboardUserBean = this.accountManager.getUserBean(UID_USER, accountUserBean);
        assertThat(dashboardUserBean.getId()).isEqualTo(UID_USER);
        assertThat(dashboardUserBean.getNom()).isEqualTo("DOE");
        assertThat(dashboardUserBean.getPrenom()).isEqualTo("John");
    }

    /**
     * Methode de test de persistUserContext.
     */
    @Test
    public void persistUserContext() {
        final DashboardUserBean userFormsBean = new DashboardUserBean(UID_USER);
        this.accountManager.persistUserContext(userFormsBean);
        assertThat(DashboardThreadContext.getUser().getId()).isEqualTo(UID_USER);
    }

    /**
     * Methode de test de cleanUserContext.
     */
    @Test
    public void cleanUserContext() {
        final DashboardUserBean userFormsBean = new DashboardUserBean(UID_USER);
        this.accountManager.persistUserContext(userFormsBean);
        this.accountManager.cleanUserContext();
        assertThat(DashboardThreadContext.getUser()).isNull();
    }

}
