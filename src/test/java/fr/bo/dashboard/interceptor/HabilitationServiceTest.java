package fr.bo.dashboard.interceptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.utils.test.AbstractTest;
import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;

/**
 * Tests {@link HabilitationService}.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HabilitationServiceTest.SpringConfiguration.class)
public class HabilitationServiceTest extends AbstractTest {

    /** La constante ENDPOINT. */
    protected static final String ENDPOINT = "local://api";

    /** providers. */
    protected List<?> providers;

    /** server. */
    protected Server server;

    private static final String USER_ID = "2018-02-ZNC-FNQ-49";

    @Autowired
    private HabilitationService habilitationService;

    /** Service to manage user rights. **/
    @Autowired
    private AuthorityServicesWebService authorityUserRightsRestService;

    @Autowired
    @Qualifier(value = "defaultJsonProvider")
    private JacksonJaxbJsonProvider jsonProvider;

    @Configuration
    @ImportResource({ "classpath:spring/bo-dashboard-webapp-properties-files-config.xml", "classpath:spring/test-service-context.xml" })
    public static class SpringConfiguration {

        @Bean
        public HabilitationService getHabilitationService() {
            return new HabilitationService();
        }

        @Bean(name = "defaultJsonProvider")
        public JacksonJaxbJsonProvider getJsonProvider() {
            final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });
        }

    }

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.authorityUserRightsRestService);

        this.providers = Arrays.asList(jsonProvider);

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setProviders(this.providers);
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.authorityUserRightsRestService);

        this.server = serverFactory.create();

    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             exception
     */
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Client.
     *
     * @return web client
     */
    protected WebClient client() {
        return WebClient.create(ENDPOINT, this.providers);
    }

    @Test
    public void testNoUserId() {
        final Map<String, List<String>> roles = this.habilitationService.getRoles(null);
        assertNull(roles);
    }

    @Test
    public void testMutipleRolesDefined() throws TechniqueException, FunctionalException {
        final Map<String, List<String>> rights = new HashMap<String, List<String>>();
        rights.put(USER_ID, Arrays.asList("referent", "accountant"));

        when(this.authorityUserRightsRestService.findUserRights(Mockito.anyString())).thenReturn(rights);
        final Map<String, List<String>> roles = this.habilitationService.getRoles(USER_ID);
        assertEquals(roles.size(), 2);
    }
}
