package fr.bo.dashboard.interceptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;

import org.apache.cxf.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.bo.dashboard.bean.DashboardUserBean;
import fr.bo.dashboard.context.DashboardThreadContext;

/**
 * Tests {@link AclOutInterceptor}.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class AclOutInterceptorTest {

    @InjectMocks
    private AclOutInterceptor interceptor = new AclOutInterceptor();

    @Mock
    private HabilitationService habilitationService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHeader() throws Exception {

        final String userId = "2018-12-JON-DOE-42";
        DashboardThreadContext.setUser(new DashboardUserBean(userId).setNom("John").setPrenom("DOE"));
        Message mockMessage = mock(Message.class);

        new HashMap<String, List<String>>();
        when(mockMessage.get(Message.PROTOCOL_HEADERS)).thenReturn(new HashMap<String, List<String>>());

        // Perform test
        interceptor.handleMessage(mockMessage);

        verify(this.habilitationService, times(1)).getRoles(any());
    }

    @Test
    public void testHeaderWithUserIdAsNull() throws Exception {

        DashboardThreadContext.setUser(new DashboardUserBean(null));
        Message mockMessage = mock(Message.class);
        when(mockMessage.get(Message.PROTOCOL_HEADERS)).thenReturn(new HashMap<String, List<String>>());

        // Perform test
        interceptor.handleMessage(mockMessage);

        verify(this.habilitationService, never()).getRoles(any());
    }

}
