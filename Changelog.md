
# Changelog
Ce fichier contient les modifications techniques du module.

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Modification

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| bo-dashboard-webapp-application.properties | ui.theme.default | default |

## [2.12.5.1] - 2019-11-28

Conservation des liens du menu suite à une recherche de dossier

## [2.11.12.0] - 2019-09-06

__Properties__ :

### Suppression

| fichier   |      nom      |
|-----------|:-------------:|
| bo-dashboard-webapp-links.properties | app.link.2.directory |

### Ajout

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| bo-dashboard-webapp-links.properties | app.link.2.dashboard | URL publique du Dashboard Guichet-Partenaires | My records&#124;https://backoffice-dashboard.${env}.guichet-partenaires.fr |
| bo-dashboard-webapp-links.properties | app.link.3.welcome | URL publique de Welcome Guichet-Partenaires | The available services&#124;https://welcome.${env}.guichet-partenaires.fr |


## [2.11.10.0] - 2019-08-07

### Modification
-Moderniser la page de consultation des dossiers MINE-891


## [2.11.9.0] - 2019-07-26

- Projet : [dashboard-backoffice](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice)

###  Modification
- Renommage du fichier de configuration des sources de données

__Properties__ :

[2.11.9.0](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice/tags/dashboard-backoffice-2.11.9.0)

## [2.11.8.0] - 2019-07-23

- Projet : [dashboard-backoffice](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice)

###  Ajout
- Visualiser les dossiers de plusieurs sources de données (backoffice, directory et support)

__Properties__ :

- Ajout d'un nouveau fichier "bo-dashboard-webapp-datasources.json" dont le contenu est le suivant : 
```
{
    "datasources": [
        {
            "key" : "backoffice",
            "engineUserType" : "bo",
            "label" : "My partners records",
            "publicUrl" : "https://backoffice-forms.${env}.guichet-partenaires.fr",
            "privateUrl" : "http://backoffice-forms-ws.${env}.guichet-partenaires.loc:12030/api"
        },
        {
            "key" : "directory",
            "engineUserType" : "user",
            "label" : "My configuration records",
            "publicUrl" : "https://forms-directory.${env}.guichet-partenaires.fr",
            "privateUrl" : "http://forms-directory.${env}.guichet-partenaires.loc:12030/api"
        },
        {
            "key" : "support",
            "engineUserType" : "support",
            "label" : "My support records",
            "publicUrl" : "support-nash.${env}.guichet-partenaires.fr",
            "privateUrl" : "http://nashsupport-ws.${env}.guichet-qualifications.loc:12030/api"
        }
    ]
}
```

## [2.11.6.0] - 2019-07-03

- Projet : [dashboard-backoffice](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice)

###  Ajout
- Déplacer les liens dans le bandeau bleu

__Properties__ :

- Ajout d'un nouveau fichier "bo-dashboard-webapp-links.properties"

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
| bo-dashboard-webapp-links.properties | app.link.1.home | URL publique du www côté Guichet-Entreprises | Home&#124;https://www.${env}.guichet-partenaires.fr |
| bo-dashboard-webapp-links.properties | app.link.2.profiler | URL publique de la page d'accueil des formalités de mise à jour de Directory | Services&#124;https://forms-directory.${env}.guichet-partenaires.fr |

## [2.11.3.0] - 2019-05-23

- Projet : [dashboard-backoffice](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice)

###  Ajout

### Modification

- les labels des dossiers ne sont pas affichés s'il n'y a pas de valeur pour ce label

### Suppression


## Liens

[2.11.3.0](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice/tags/dashboard-backoffice-2.11.3.0)


## [2.11.1.0] - 2019-04-30

- Projet : [dashboard-backoffice](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice)

###  Ajout

- Nouveau fichier _Changelog.md_
- bo_record.html : ajout des filtres de recherche dans l'interface utilisateur

### Modification

- __pom.xml__ : nouvelle version pour _nash-webservices_
- adaptation de DashboardController pour prendre en compte les filtres de recherche
- Tests Unitaires adaptés à cette évolution

### Suppression


## Liens

[2.11.1.0](https://tools.projet-ge.fr/gitlab/apps/dashboard-backoffice/tags/dashboard-backoffice-2.11.1.0)

